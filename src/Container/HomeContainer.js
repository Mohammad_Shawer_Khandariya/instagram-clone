import { connect } from "react-redux";
import Home from "../components/Home/Home";
import { getData } from "../Service/Action/Action";

const mapStateToProps = (state) => ({
    data: state,
});

const mapDispatchToProps = (dispatch) => ({
    getPostsData: (data) => dispatch(getData(data)),
})

export default connect( mapStateToProps, mapDispatchToProps)(Home);