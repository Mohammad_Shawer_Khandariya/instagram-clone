import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom/cjs/react-router-dom.min';
import './App.css';
// import Navbar from './components/Navbar/Navbar';
import Navbar from './Container/NavbarContainer';
// import Home from './components/Home/Home';
import Home from "./Container/HomeContainer";
import Explore from './components/Explore/Explore';
import Create from './components/Create/Create';
import Profile from './components/Profile/Profile';
import UserProfile from './UserProfile/UserProfile';
import Signup from './components/Authorise/Signup';
import Signin from './components/Authorise/SignIn';

function App() {
  return (
    // <div className="app bg-light">
      <Router>
      <Navbar />
      {/* story hidden */}
      <Switch>
        <div className='bg-light'>
        <Route exact path="/"><Home /></Route>
        <Route exact path="/direct"></Route>
        <Route exact path="/create"><Create /></Route>
        <Route exact path="/explore"><Explore /></Route>
        <Route exact path="/profile"><Profile /></Route>
        <Route exact path="/user-profile/:id"><UserProfile /></Route>
        <Route exact path="/signIn"><Signin /></Route>
        <Route exact path="/signUp"><Signup /></Route>
        </div>
      </Switch>
      </Router>
    // </div>
  );
}

export default App;
