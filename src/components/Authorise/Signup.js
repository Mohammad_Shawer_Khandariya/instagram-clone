import React from "react";
import { Link } from "react-router-dom";
import logo from "../../assets/logo.png";

export default class Signup extends React.Component {

    render() {
        return (
            <div className="container mx-auto text-center" style={{"width": "350px"}}>
                <div className="bg-white p-3 rounded border border-secondary">
                <img src={logo} alt="Brand Logo" />
                <p>Sign up to see photos and videos from your friends.</p>
                <button className="btn btn-primary">Log in with Faceboook</button>
                <p>OR</p>

                <form onSubmit={() => this.submit()}>
                    <input type="email" placeholder="email address" className="w-75 my-1" />
                    <input type="text" placeholder="Full Name" className="w-75 my-1" />
                    <input type="text" placeholder="Username" className="w-75 my-1" />
                    <input type="text" placeholder="Password" className="w-75 my-1" />
                    <input type="submit" value="Sign Up" className="w-75 my-1 btn btn-primary"/>
                </form>
                <p>By signing up, you agree to our <b>Terms, Data Policy</b> and <b>Cookie Policy</b>.</p>
                </div>
                <div className="my-2 bg-white p-3 rounded border border-secondary">
                    <span>Have an account?</span><Link to="/signIn" className="ms-1" >Log in</Link>
                </div>
            </div>
        )
    }
}