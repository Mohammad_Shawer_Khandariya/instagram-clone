import React from "react";
import { Link } from "react-router-dom";
import logo from "../../assets/logo.png";

export default class Signin extends React.Component {

    render() {
        return (
            <div className="container mx-auto text-center" style={{"width": "350px"}}>
                <div className="bg-white rounded border border-secondary p-3">
                <img src={logo} alt="Brand Logo" />
                <form onSubmit={() => this.onSubmit()}>
                    <input type="email" placeholder="email address" className="w-75 my-2" />
                    <input type="password" placeholder="password" className="w-75 my-2" />
                    <Link to="/"><input type="submit" value="Log In" className="w-75 my-2 btn btn-primary"/></Link>
                </form>
                <p>OR</p>
                <p>Log in with Facebook</p>
                <p>Forgotten your password?</p>
                </div>
                <div className="bg-white rounded border border-secondary my-2 p-2">
                    <span>Don't have an account?</span><Link to="/signUp" className="ms-1">Sign up</Link>
                </div>
            </div>
        )
    }
}