import React from "react";
import Signup from "../Authorise/Signup";
import Signin from "../Authorise/SignIn";

export default class Authorisation extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            showSignUp: true,
        }
    }


    render() {
        return (
            <div>
                {this.state.showSignUp?
                <Signup />
                :
                <Signin />
            }
            </div>
        )
    }
}