import React from "react";
import './Home.css';
import { Link } from "react-router-dom";
import { FaRegComment } from 'react-icons/fa';
import { AiOutlineHeart } from 'react-icons/ai';
import {FiShare} from 'react-icons/fi';
import {BsBookmark} from 'react-icons/bs';

class Home extends React.Component {
    render() {
        console.log(this.props.data.reducer.users);
        console.log(this.props.data.reducer.posts);
        const users = this.props.data.reducer.users;
        const posts = this.props.data.reducer.posts;
        return (
            <div className="container w-100">
                {
                    users.length === 0 ?
                        <h1>Data not found!</h1>
                        :
                        users.map((user) => {
                            return (
                                <div className='w-100 container my-5' key={user.id} id="user">
                                    <div className="card d-flex flex-column">
                                        <header className='header d-flex flex-row align-items-center justify-content-between p-3'>
                                            <div className='d-flex flex-row align-items-center'>
                                                <img className='user-photo' src={user.image} alt="User Profile" />
                                                <Link to={`/user-profile/${user.id}`} className='username' href="#user">{user.username}</Link>
                                            </div>
                                            <a className='more' href="#user">...</a>
                                        </header>
                                        {
                                            (posts.length === 0) ?
                                                <p>Posts not found!</p>
                                                :
                                                posts.filter((post) => post.userId === user.id).slice(0, 1).map((post) => {
                                                    return (
                                                        <div key={post.userId}>
                                                            <img className='image' src={post.image} alt="User Post" />
                                                            <div className='description p-4 d-flex flex-column'>

                                                                <div className='buttons d-flex flex-row justify-content-between'>
                                                                    <div className='d-flex flex-row '>
                                                                        <AiOutlineHeart />
                                                                        <FaRegComment />
                                                                        <FiShare />
                                                                    </div>
                                                                    <BsBookmark />
                                                                </div>

                                                                <div className='likes'>
                                                                    {Math.floor(Math.random() * 10000)} likes
                                                                </div>

                                                                <div className='caption'>
                                                                    <a className='username' href="#user">{user.username}</a>
                                                                    <span>{post.title}</span>
                                                                </div>

                                                                <div className="comments">
                                                                    <a href="#user"> View all 4 comments</a>
                                                                </div>

                                                                <div className='upload-time'>
                                                                    7 minutes ago
                                                                </div>

                                                            </div>

                                                            <div className='add-comment d-flex px-4 py-3 align-items-center justify-content-between'>
                                                                <i className="fa fa-heart emoji"></i>
                                                                <input className='comment-field' type="text" placeholder='Add a comment' />
                                                                <button className='post-btn'> POST</button>
                                                            </div>
                                                        </div>
                                                    )
                                                })
                                        }
                                    </div>
                                </div>
                            )
                        })
                }
            </div>
        )
    }
}
export default Home;