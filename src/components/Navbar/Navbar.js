import React from "react";
import logo from "../../assets/logo.png";
import './Navbar.css';
import { Link } from "react-router-dom";
import { AiOutlineHome, AiOutlinePlusCircle, AiOutlineHeart, AiOutlineUser } from 'react-icons/ai';
import { RiMessengerLine } from 'react-icons/ri';
import { MdOutlineExplore } from 'react-icons/md';

export default class Navbar extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      searchValue: "",
      searchedUser: [],
    }
  }

  searchUser(event) {
    const value = event.target.value;
    this.setState({
      searchValue: value,
    })

    const userData = this.props.data.reducer.users;
    const filterData = userData.filter((user) => {
      if (user.name.toLowerCase().includes(value.toLowerCase())) {
        return true;
      }
    })

    if (value.length === 0) {
      this.setState({
        searchedUser: [],
      })
    } else {
      this.setState({
        searchedUser: filterData,
      })
    }
  }

  clearSearch() {
    this.setState({
      searchValue: "",
      searchedUser: [],
    })
  }

  render() {
    // console.log(this.props.data.reducer.users);
    return (
      <div className="w-100 border-bottom border-secondary fixed-top bg-white" id="navbar">
        <nav className="container w-50 d-flex flex-column flex-md-row align-items-center justify-content-between py-2 mx-auto">
          <a href="#navbar" className="w-20">
            <Link to="/"><img src={logo} alt="Instagram Logo" width="100vw" /></Link>
          </a>
          <div className="d-none d-sm-none d-lg-block">
            <input className="w-100" type="text"
             placeholder="Search" onChange={(event) => this.searchUser(event)}
            value={this.state.searchValue}/>
              {
                (this.state.searchedUser.length)?
                <div className='showData border border-secondary bg-white'>
                {this.state.searchedUser.map((user) => {
                  return (
                    <>
                      <Link to={`/user-profile/${user.id}`} onClick={() => this.clearSearch()}>{user.name}</Link>
                    </>
                  )
                })}
                </div>
                :
                ""
              }
          </div>
          <div className="list-group list-group-horizontal">
            <a href="#navbar" className="list-group-item border-0">
              <Link to="/"><AiOutlineHome className="icon " /></Link>
            </a>
            <a href="#navbar" className="list-group-item border-0">
              <Link to="/direct"><RiMessengerLine className="icon " /></Link>
            </a>
            <a href="#navbar" className="list-group-item border-0">
              <Link to="/create"><AiOutlinePlusCircle className="icon" /></Link>
            </a>
            <a href="#navbar" className="list-group-item border-0">
              <Link to="/explore"><MdOutlineExplore className="icon" /></Link>
            </a>
            <a href="#navbar" className="list-group-item border-0">
              <AiOutlineHeart className="icon" />
            </a>
            <a href="#navbar" className="list-group-item border-0">
              <div className="btn-group icon">
                <AiOutlineUser className="dropdown-toggle" data-bs-toggle="dropdown" data-bs-display="static" aria-expanded="false" />
                <ul className="dropdown-menu dropdown-menu-end dropdown-menu-lg-start">
                  <Link to="/profile" className="dropdown-item" >Profile</Link>
                  <li className="dropdown-item">Saved</li>
                  <li className="dropdown-item">Settings</li>
                  <li className="dropdown-item border-top"><Link to="/signIn" >Log out</Link></li>
                </ul>
              </div>
            </a>
          </div>
        </nav>
      </div>
    );
  }
}