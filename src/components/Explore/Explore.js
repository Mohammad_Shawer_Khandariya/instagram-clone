import React from "react";

export default class Explore extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            posts: [['https://images.pexels.com/photos/1257860/pexels-photo-1257860.jpeg?cs=srgb&dl=pexels-philippe-donn-1257860.jpg&fm=jpg',
            'https://source.unsplash.com/1200x900/?mountain,forest',
                'https://source.unsplash.com/1200x900/?boy,water'],
            ['https://source.unsplash.com/1200x900/?ice,water',
                'https://source.unsplash.com/1200x900/?fire,water',
                'https://source.unsplash.com/1200x900/?ice,fire'],
            ['https://source.unsplash.com/1200x900/?stone,water',
                'https://source.unsplash.com/1200x900/?mountain,water',
                'https://source.unsplash.com/1200x900/?ice,mountain',
                'https://source.unsplash.com/1200x900/?mountain,tree'],
            ['https://source.unsplash.com/1200x900/?mountain,forest',
                'https://source.unsplash.com/1200x900/?forest,fire',
                'https://source.unsplash.com/1200x900/?winter,ice'],
            ['https://source.unsplash.com/1200x900/?boy,water',
                'https://source.unsplash.com/1200x900/?ice,water',
                'https://source.unsplash.com/1200x900/?fire,water'],
            ['https://source.unsplash.com/1200x900/?ice,fire',
                'https://source.unsplash.com/1200x900/?fire,water',
                'https://source.unsplash.com/1200x900/?mountain,water'],
            ['https://source.unsplash.com/1200x900/?ice,mountain',
                'https://source.unsplash.com/1200x900/?mountain,tree',
                'https://source.unsplash.com/1200x900/?mountain,forest'],
            ['https://source.unsplash.com/1200x900/?forest,fire',
                'https://source.unsplash.com/1200x900/?winter,ice',
                'https://source.unsplash.com/1200x900/?boy,water'],
            ['https://source.unsplash.com/1200x900/?ice,water',
                'https://source.unsplash.com/1200x900/?fire,water',
                'https://source.unsplash.com/1200x900/?ice,fire'],
            ['https://source.unsplash.com/1200x900/?fire,water',
                'https://source.unsplash.com/1200x900/?mountain,water',
                'https://source.unsplash.com/1200x900/?ice,mountain'],
            ['https://source.unsplash.com/1200x900/?mountain,tree',
                'https://source.unsplash.com/1200x900/?mountain,forest',
                'https://source.unsplash.com/1200x900/?forest,fire']
            ],
        }
    }


    render() {
        return (
            <div className="container" >
                {
                    this.state.posts.map((el, index) => {
                        return (
                            <div className="row" key={index}>
                                <div className="col-6">
                                <div className="" key={index}>
                                    <img src={el[0]} alt="Random"height="250vh" className="w-100 border border-light" />
                                </div>
                                <div className="" key={index}>
                                    <img src={el[1]} alt="Random" height="250vh" className="w-100 border border-light" />
                                </div>
                                </div>
                                <div className="col-6" key={index}>
                                    <img src={el[2]} alt="Random" height="500vh" className="w-100 border border-light" />
                                </div>
                            </div>
                        )
                    })
                }
            </div>
        )
    }
}