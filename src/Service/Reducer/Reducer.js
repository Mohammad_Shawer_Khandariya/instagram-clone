import { users } from "../users";
import { posts } from "../posts"

const initialState = {
    users: users,
    posts: posts,
    comments: [],
}

export default function reducer(state = initialState, action) {
    switch(action.type) {
        case "GET_DATA":
            return {
                ...state, 
                users:action.data.users,
                posts: action.data.posts,
                comments: action.data.comments,
            }

        default: return state;
    }
}