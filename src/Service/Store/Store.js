import {createStore} from "redux";
import CombinedReducers from "../CombinedReducer/CombinedReducer";

const store = createStore(CombinedReducers);

export default store;