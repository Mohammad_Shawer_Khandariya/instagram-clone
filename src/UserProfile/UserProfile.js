import React from "react";
import "./UserProfile.css";

export default class Profile extends React.Component {
            
    render() {
        return (
            <div className="d-flex flex-column" style={{ gap: "30px" }}>
                <div className=" w-50 mx-auto d-flex flex-row align-items-center justify-content-around">
                    <div>
                        <img src='https://images.pexels.com/photos/1257860/pexels-photo-1257860.jpeg?cs=srgb&dl=pexels-philippe-donn-1257860.jpg&fm=jpg' className="profile" alt="Profile Pic"/>
                    </div>
                    <div>
                        <div className="d-flex flex-row" style={{ gap: "10px" }}>
                            <h3><b>UserName</b></h3>
                            <button className="btn btn-primary">Follow</button>
                        </div>
                        <div className="d-flex flex-row flex-wrap my-3" style={{ gap: "30px" }}>
                            <span><b>{Math.floor(Math.random() * 100)}</b> posts</span>
                            <span><b>{Math.floor(Math.random() * 1000)}</b> followers</span>
                            <span><b>{Math.floor(Math.random() * 500)}</b> following</span>
                        </div>
                        <div className="d-flex flex-column">
                            <span><b>Name</b></span>
                            <span>Bio</span>
                        </div>
                    </div>
                </div>
                <div className="d-flex flex-row justify-content-center" style={{ gap: "20px" }}>
                    <div className="d-flex flex-column align-items-center">
                        <img src='https://source.unsplash.com/1200x900/?mountain,forest' className="status" alt="post" />
                        <span>Mountain and Forest</span>
                    </div>
                    <div className="d-flex flex-column align-items-center">
                        <img src='https://source.unsplash.com/1200x900/?boy,water' className="status" alt="post" />
                        <span>Water and Ice</span>
                    </div>
                    <div className="d-flex flex-column align-items-center">
                        <img src='https://source.unsplash.com/1200x900/?ice,forest' className="status" alt="post" />
                        <span>Forest</span>
                    </div>
                </div>
                <div className="d-flex flex-column" style={{ gap: "10px" }}>
                    <div className="d-flex flex-row justify-content-center mx-auto" style={{ gap: "10px", width: "fit-content" }}>
                        <span style={{ borderBottom: "1px solid grey" }}>Posts</span>
                        <span>Tagged</span>
                    </div>
                    <div className="d-flex flex-row flex-wrap justify-content-center" style={{ gap: "10px" }}>
                        <img src='https://source.unsplash.com/1200x900/?boy,water' className="posts" alt="post" />
                        <img src='https://source.unsplash.com/1200x900/?mountain,forest' className="posts" alt="post" />
                        <img src='https://images.pexels.com/photos/1257860/pexels-photo-1257860.jpeg?cs=srgb&dl=pexels-philippe-donn-1257860.jpg&fm=jpg' className="posts" alt="post" />
                        <img src='https://source.unsplash.com/1200x900/?ice,forest' className="posts" alt="post" />
                        <img src='https://source.unsplash.com/1200x900/?fire,forest' className="posts" alt="post" />
                    </div>
                </div>
            </div>
        )
    }
}